<?php

use GamingPlatform\Lib\HTTP\GuzzleStrategy;

const SRC_DIRECTORY = __DIR__ . DIRECTORY_SEPARATOR . 'src';
const VIEWS_DIRECTORY = SRC_DIRECTORY . DIRECTORY_SEPARATOR . 'Views';

// Autoloader
require_once __DIR__ . '/vendor/autoload.php';

$diService = new DependencyInjectionService();

// Register config into dependency container as singleton
$diService->register('config', static function() {
    $configFile = __DIR__ . '/config.ini';

    return (
        is_readable($configFile) &&
        ($config = parse_ini_file($configFile, true))
    )
        ? (object)$config
        : (object)[];
});

// Register HTTP client into dependency container
$diService->register(
    'http',
    static function() {
        // Here we can choose whatever http library we like,
        // and create a strategy class, so we get unified return data
        // no matter of the client
        return new GamingPlatform\Lib\HTTP\HTTP(new GuzzleStrategy());
    },
    false
);

