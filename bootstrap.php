<?php

use GamingPlatform\Controllers\HomeController;
use GamingPlatform\Lib\Generic\Exceptions\ControllerException;
use GamingPlatform\Lib\Generic\View;

require_once __DIR__ . '/basics.php';

/**
 * Generic function for showing an error page
 *
 * @param string $message
 * @param string $number
 * @param bool $userVisible
 *
 * @return void
 */
function gaming_platform_error_page(string $message = '', string $number = '404', bool $userVisible = false)
{
    http_response_code($number);

    $data = [
        'message' => ($userVisible || !isset($_ENV['APP_ENV']) || $_ENV['APP_ENV'] !== 'production')
            ? $message
            : 'Unexpected issue. Call the administration please!'
    ];

    $viewFile = VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . 'Main' . DIRECTORY_SEPARATOR . 'error.php';
    require VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . 'Main' . DIRECTORY_SEPARATOR . 'page.php';
    exit();
}

// Custom routing
try {
    if (!empty($_GET['section']) && $_GET['action']) {
        // Load the controller file
        $controllerClass = '\GamingPlatform\Controllers\\' . ucfirst(strtolower($_GET['section'])) . 'Controller';

        if (!class_exists($controllerClass)) {
            gaming_platform_error_page('Not found!', 404);
        }

        $controller = new $controllerClass();

        if (!method_exists($controller, $_GET['action'])) {
            gaming_platform_error_page('Not found!', 404);
        }

        // Fire the action and get the result data
        $data = $controller->{$_GET['action']}();
        $template = $_GET['action'];
    } else {
        $data = (new HomeController())->index();
        $template = 'home';
    }

    // Show the HTML
    View::render($template, $data);
} catch (ControllerException $e) {
    gaming_platform_error_page(
        $e->getMessage(),
        500,
        $e->getCode() === ControllerException::VISIBILITY_USER
    );
} catch (Exception $e) {
    gaming_platform_error_page($e->getMessage(), 500);
}
