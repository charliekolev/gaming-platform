<?php

namespace GamingPlatform\Controllers;

use GameInterface;
use GameReturnStatus;
use GamingPlatform\Lib\Generic\AbstractController;
use GamingPlatform\Lib\Generic\Exceptions\ControllerException;
use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\Results\LoseResult;
use GamingPlatform\Lib\HTTP\Results\StakeResult;
use GamingPlatform\Lib\HTTP\Results\WinResult;
use GamingPlatform\Lib\HTTP\Senders\LoseRequestSenderCreator;
use GamingPlatform\Lib\HTTP\Senders\StakeRequestSenderCreator;
use GamingPlatform\Lib\HTTP\Senders\WinRequestSenderCreator;
use JsonException;
use Pheanstalk\Pheanstalk;
use SlotMachine;

class HomeController extends AbstractController
{
    /**
     * @return array
     */
    public function index(): array
    {
        $stakeAmount = new Money(50, 'EUR');
        $account = 'My Account';
        $operator = 'operator1';
        $return = 'Oops';

        $senderCreator = new StakeRequestSenderCreator();

        if ($sender = $senderCreator->create($operator)) {
            $result = $sender->send($stakeAmount, $account);

            // Stake succeeded
            if ($result->getStatus() === StakeResult::RESULT_SUCCESS) {
                $status = $this->playTheGame(new SlotMachine());
                $failed = false;

                if ($status->getStatus() === GameReturnStatus::GAME_STATUS_WIN) {
                    $senderCreator = new WinRequestSenderCreator();

                    if ($sender = $senderCreator->create($operator)) {
                        $amount = $status->getAmount();
                        $winResult = $sender->send($amount->getAmount());
                        $status = $winResult->getStatus();

                        /*
                         * Here could have some extra process, and something wrong could happen here.
                         */
                        $failed = true;

                        if (!$failed) {
                            if ($status === WinResult::RESULT_SUCCESS) {
                                $return = sprintf('You won %f %s !', $amount->getAmount(), $amount->getCurrency());
                            } elseif ($status === WinResult::RESULT_ERROR) {
                                // TODO
                            } else {
                                // Retry
                                $failed = true;
                            }
                        }
                    } else {
                        // TODO: not existing operator used in the win request
                    }
                } else {
                    // Losing the game
                    $senderCreator = new LoseRequestSenderCreator();

                    if ($sender = $senderCreator->create($operator)) {
                        $loseResult = $sender->send();
                        $status = $loseResult->getStatus();

                        /*
                         * Here could have some extra process, and something wrong could happen here.
                         */
                        $failed = true;

                        if (!$failed) {
                            if ($status === LoseResult::RESULT_SUCCESS) {
                                $return = 'You have lost the game !';
                            } elseif ($status === WinResult::RESULT_ERROR) {
                                // TODO
                            } else {
                                // Retry
                                $failed = true;
                            }
                        }
                    } else {
                        // TODO: not existing operator used in the lose request
                    }
                }

                if ($failed) {
                    try {
                        if (
                            ($conf = $this->config()) &&
                            !empty($conf['pheanstalk']['host']) &&
                            !empty($conf['pheanstalk']['reconciliation_tube']) &&
                            isset($conf['pheanstalk']['operators'][$operator])
                        ) {
                            $pheanstalk = Pheanstalk::create($conf['pheanstalk']['host']);
                            // TODO: payload
                            $data = [
                                'operator' => $operator,
                            ];

                            // Queue a Reconciliation job
                            $pheanstalk
                                ->useTube($conf['pheanstalk']['reconciliation_tube'])
                                ->put(json_encode(['data' => $data], JSON_THROW_ON_ERROR));
                        }
                    } catch (DependencyNotFoundException|ControllerException|JsonException $e) {
                        // TODO: return money to the user
                    }
                }
            } else {
                //TODO: Stake failed
            }
        } else {
            // TODO: not existing operator used in the stake request
        }

        return [
            'result' => $return,
        ];
    }

    private function playTheGame(GameInterface $game): GameReturnStatus
    {
        return $game->play();
    }
}
