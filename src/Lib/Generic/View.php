<?php

namespace GamingPlatform\Lib\Generic;

class View
{
    /**
     * Render the content of a template.
     *
     * @param string $template
     * @param array $data
     *
     * @return void
     */
    public static function render(string $template, array $data = []): void
    {
        if (is_array($data)) {
            $viewFile = VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . $template . '.php';

            if (!file_exists($viewFile)) {
                gaming_platform_error_page('Not found!', 404);
            }

            // Show the HTML
            require VIEWS_DIRECTORY . DIRECTORY_SEPARATOR . 'Main' . DIRECTORY_SEPARATOR . 'page.php';
        } elseif (is_string($data)) {
            echo $data;
        } elseif (!is_null($data)) {
            gaming_platform_error_page('Controller returns wrong data!', 500);
        }
    }
}
