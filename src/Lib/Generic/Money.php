<?php

namespace GamingPlatform\Lib\Generic;

class Money
{
    private float $amount;
    private string $currency;

    /**
     * Money class constructor
     *
     * @param float $amount
     * @param string $currency
     */
    public function __construct(float $amount, string $currency = 'BGN')
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }
}
