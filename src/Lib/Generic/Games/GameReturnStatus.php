<?php

use GamingPlatform\Lib\Generic\Money;

class GameReturnStatus
{
    public const GAME_STATUS_LOSS = 0;
    public const GAME_STATUS_WIN = 1;

    private int $status;
    private Money $amount;

    /**
     * @param int $status
     * @param Money $amount
     */
    public function __construct(int $status = self::GAME_STATUS_LOSS, Money $amount)
    {
        $this->status = $status;
        $this->amount = $amount;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}
