<?php

interface GameInterface
{
    /**
     * Fires the game
     *
     * @return GameReturnStatus
     */
    public function play(): GameReturnStatus;
}
