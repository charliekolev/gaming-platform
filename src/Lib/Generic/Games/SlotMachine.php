<?php

use GamingPlatform\Lib\Generic\Money;

class SlotMachine implements GameInterface
{
    /**
     * @inheritDoc
     */
    public function play(): GameReturnStatus
    {
        /*
         * Some game play logic here
         */

        $status = GameReturnStatus::GAME_STATUS_WIN;
        $amount = new Money(50, 'EUR');

        return new GameReturnStatus($status, $amount);
    }
}
