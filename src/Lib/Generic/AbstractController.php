<?php

namespace GamingPlatform\Lib\Generic;

use GamingPlatform\Lib\DependencyInjection\DependencyInjectionService;
use GamingPlatform\Lib\DependencyInjection\DependencyNotFoundException;
use GamingPlatform\Lib\Generic\Exceptions\ControllerException;
use GamingPlatform\Lib\HTTP\HTTP;

/**
 * This is the base controller class
 */
abstract class AbstractController
{
    /**
     * Get the configuration object
     *
     * @return object
     * @throws ControllerException
     */
    protected function config(): object
    {
        try {
            return (new DependencyInjectionService())->get('config');
        } catch (DependencyNotFoundException $e) {
            throw new ControllerException($e->getMessage());
        }
    }

    /**
     * Get the HTTP object
     *
     * @return HTTP
     * @throws ControllerException
     */
    protected function http(): HTTP
    {
        try {
            return (new DependencyInjectionService())->get('http');
        } catch (DependencyNotFoundException $e) {
            throw new ControllerException($e->getMessage());
        }
    }
}
