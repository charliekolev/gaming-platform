<?php

namespace GamingPlatform\Lib\HTTP;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * This class uses Guzzle library for sending of HTTP messages
 */
class GuzzleStrategy implements HTTPStrategyInterface
{
    /**
     * @inheritDoc
     */
    public function send(HTTPRequest $request): HTTPResult
    {
        $method = $request->getMethod();
        $options = [];

        if ($headers = $request->getHeaders()) {
            $options['headers'] = $headers;
        }

        if (($params = $request->getParams()) !== null) {
            if (!is_array($params)) {
                $key = 'body';
            } elseif ($method === 'GET') {
                $key = 'query';
            } elseif ($request->getIsJson()) {
                $key = 'json';

            } else {
                $key = 'form_params';
            }

            $options[$key] = $params;
        }

        try {
            $client = new Client();
            $response = $client->request($method, $request->getURL(), $options);

            return (new HTTPResult())
                ->setBody($response->getBody())
                ->setStatus($response->getStatusCode());
        } catch (GuzzleException $e) {
            // Here we can catch different Guzzle types of exceptions and change the code
            throw new HTTPException($e->getMessage(), HTTPException::GENERIC_EXCEPTION);
        }
    }
}
