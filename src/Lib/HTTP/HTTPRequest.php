<?php

namespace GamingPlatform\Lib\HTTP;

class HTTPRequest
{
    private string $method = 'GET';
    private ?string $url = null;
    private ?array $headers = null;
    private $params = null;
    private bool $isJson = false;

    /**
     * Set the HTTP request method
     *
     * @param string $method
     *
     * @return $this
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;
        return $this;
    }

    /**
     * Set the HTTP request URL
     *
     * @param string $url
     *
     * @return $this
     */
    public function setURL(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Set the HTTP request headers
     *
     * @param array $headers
     *
     * @return $this
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * Set the HTTP request params
     *
     * @param string|array $params
     *
     * @return $this
     */
    public function setParams($params): self
    {
        $this->params = $params;
        return $this;
    }

    /**
     * Set weather it is a JSON HTTP request
     *
     * @param bool $isJson
     * @return $this
     */
    public function setIsJson(bool $isJson): self
    {
        $this->isJson = $isJson;
        return $this;
    }

    /**
     * Get HTTP request method
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get HTTP request URL
     *
     * @return string
     */
    public function getURL(): string
    {
        return $this->url;
    }

    /**
     * Get HTTP request headers
     *
     * @return array|null
     */
    public function getHeaders(): ?array
    {
        return $this->headers;
    }

    /**
     * Get HTTP request params
     *
     * @return string|array|null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Get whether it is a JSON HTTP request
     *
     * @return bool
     */
    public function getIsJson(): bool
    {
        return $this->isJson;
    }
}
