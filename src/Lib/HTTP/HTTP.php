<?php

namespace GamingPlatform\Lib\HTTP;

/**
 * Sends HTTP messages via specified HTTP client
 */
class HTTP
{
    private HTTPStrategyInterface $client;

    /**
     * @param HTTPStrategyInterface $client
     */
    public function __construct(HTTPStrategyInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Send HTTP message using any provider
     * that implements HTTPStrategyInterface interface
     *
     * @param HTTPRequest $request
     *
     * @return HTTPResult
     * @throws HTTPException
     */
    public function send(HTTPRequest $request): HTTPResult
    {
        /*
         * Here we can put some cool logic before or after calling the send method of the client
         */
        return $this->client->send($request);
    }
}
