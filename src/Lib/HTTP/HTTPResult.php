<?php

namespace GamingPlatform\Lib\HTTP;

class HTTPResult
{
    private int $status;
    private $body;

    /**
     * Set the HTTP status
     *
     * @param int $status
     *
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Set the HTTP body
     *
     * @param $body
     *
     * @return $this
     */
    public function setBody($body): self
    {
        $this->body = $body;
        return $this;
    }

    /**
     * Get HTTP return status code
     *
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status ?? null;
    }

    /**
     * Get HTTP body
     *
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }
}
