<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\HTTP\HTTPException;
use GamingPlatform\Lib\HTTP\Requests\ReconcileRequestCreator;
use GamingPlatform\Lib\HTTP\Results\ReconcileResult;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\ReconcileRequestSenderInterface;
use GamingPlatform\Lib\HTTP\Traits\HTTPSendTrait;

class Operator1ReconcileRequestSender implements ReconcileRequestSenderInterface
{
    use HTTPSendTrait;
    /**
     * Send request to Operator 1
     *
     * @inheritDoc
     */
    public function send(): ReconcileResult
    {
        $requestCreator = new ReconcileRequestCreator();
        $request = $requestCreator->create('operator1');

        try {
            $result = $this->call($request);
        } catch (HTTPException $e) {
            return ReconcileResult::RESULT_ERROR;
        }

        // Different Operator may return different statuses, and here we map it to unified ones
        $map = [
            'success' => ReconcileResult::RESULT_SUCCESS,
            'error' => ReconcileResult::RESULT_ERROR,
            'retry' => ReconcileResult::RESULT_RETRY,
        ];

        // As well the structure can be different between the Operators,
        // so here we fetch the information depending on the structure this Operator offers.
        // Then I put it in a unified result
        return (new ReconcileResult())
            ->setStatus((
                $result->getStatus() === 200 &&
                ($body = $result->getBody()) &&
                ($body = json_decode($body, true)) &&
                isset($body['status'], $map[$body['status']])
            ) ? $map[$body['status']] : ReconcileResult::RESULT_ERROR);
    }
}
