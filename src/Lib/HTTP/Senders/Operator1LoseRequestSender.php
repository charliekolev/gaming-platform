<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\HTTP\HTTPException;
use GamingPlatform\Lib\HTTP\Requests\LoseRequestCreator;
use GamingPlatform\Lib\HTTP\Results\LoseResult;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\LoseRequestSenderInterface;
use GamingPlatform\Lib\HTTP\Traits\HTTPSendTrait;

class Operator1LoseRequestSender implements LoseRequestSenderInterface
{
    use HTTPSendTrait;
    /**
     * Send request to Operator 1
     *
     * @inheritDoc
     */
    public function send(): LoseResult
    {
        $requestCreator = new LoseRequestCreator();
        $request = $requestCreator->create('operator1');

        try {
            $result = $this->call($request);
        } catch (HTTPException $e) {
            return LoseResult::RESULT_ERROR;
        }

        // Different Operator may return different statuses, and here we map it to unified ones
        $map = [
            'success' => LoseResult::RESULT_SUCCESS,
            'error' => LoseResult::RESULT_ERROR,
            'retry' => LoseResult::RESULT_RETRY,
        ];

        // As well the structure can be different between the Operators,
        // so here we fetch the information depending on the structure this Operator offers.
        // Then I put it in a unified result
        return (new LoseResult())
            ->setStatus((
                $result->getStatus() === 200 &&
                ($body = $result->getBody()) &&
                ($body = json_decode($body, true)) &&
                isset($body['status'], $map[$body['status']])
            ) ? $map[$body['status']] : LoseResult::RESULT_ERROR);
    }
}
