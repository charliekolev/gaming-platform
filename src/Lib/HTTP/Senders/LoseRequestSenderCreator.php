<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\HTTP\Senders\Interfaces\LoseRequestSenderCreatorInterface;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\LoseRequestSenderInterface;

class LoseRequestSenderCreator implements LoseRequestSenderCreatorInterface
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1LoseRequestSender::class,
        // ....
    ];

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?LoseRequestSenderInterface
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return new $className();
        } else {
            return null;
        }
    }
}
