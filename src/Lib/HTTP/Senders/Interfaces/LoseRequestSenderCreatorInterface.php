<?php

namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

interface LoseRequestSenderCreatorInterface
{
    /**
     * Create a request sender for a specified operator
     *
     * @param string $operator
     *
     * @return LoseRequestSenderInterface|null
     */
    public function create(string $operator): ?LoseRequestSenderInterface;
}
