<?php
namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\Results\WinResult;

interface WinRequestSenderInterface
{
    /**
     * Send HTTP request
     *
     * @param Money $amount
     *
     * @return WinResult
     */
    public function send(Money $amount): WinResult;
}
