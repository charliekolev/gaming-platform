<?php
namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\Results\StakeResult;

interface StakeRequestSenderInterface
{
    /**
     * Send HTTP request
     *
     * @param Money $amount
     * @param string $account
     * @return StakeResult
     */
    public function send(Money $amount, string $account): StakeResult;
}
