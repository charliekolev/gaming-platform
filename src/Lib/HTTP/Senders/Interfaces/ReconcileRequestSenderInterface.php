<?php
namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

use GamingPlatform\Lib\HTTP\Results\ReconcileResult;

interface ReconcileRequestSenderInterface
{
    /**
     * Send HTTP request
     *
     * @return ReconcileResult
     */
    public function send(): ReconcileResult;
}
