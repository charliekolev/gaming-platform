<?php

namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

interface ReconcileRequestSenderCreatorInterface
{
    /**
     * Create a request sender for a specified operator
     *
     * @param string $operator
     *
     * @return ReconcileRequestSenderInterface|null
     */
    public function create(string $operator): ?ReconcileRequestSenderInterface;
}
