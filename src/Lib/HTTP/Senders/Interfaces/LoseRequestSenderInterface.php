<?php
namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

use GamingPlatform\Lib\HTTP\Results\LoseResult;

interface LoseRequestSenderInterface
{
    /**
     * Send HTTP request
     *
     * @return LoseResult
     */
    public function send(): LoseResult;
}
