<?php

namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

interface WinRequestSenderCreatorInterface
{
    /**
     * Create a request sender for a specified operator
     *
     * @param string $operator
     *
     * @return WinRequestSenderInterface|null
     */
    public function create(string $operator): ?WinRequestSenderInterface;
}
