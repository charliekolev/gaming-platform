<?php

namespace GamingPlatform\Lib\HTTP\Senders\Interfaces;

interface StakeRequestSenderCreatorInterface
{
    /**
     * Create a request sender for a specified operator
     *
     * @param string $operator
     *
     * @return StakeRequestSenderInterface|null
     */
    public function create(string $operator): ?StakeRequestSenderInterface;
}
