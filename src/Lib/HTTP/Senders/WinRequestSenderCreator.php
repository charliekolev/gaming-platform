<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\HTTP\Senders\Interfaces\WinRequestSenderCreatorInterface;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\WinRequestSenderInterface;

class WinRequestSenderCreator implements WinRequestSenderCreatorInterface
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1WinRequestSender::class,
        // ....
    ];

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?WinRequestSenderInterface
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return new $className();
        } else {
            return null;
        }
    }
}
