<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\HTTPException;
use GamingPlatform\Lib\HTTP\Requests\StakeRequestCreator;
use GamingPlatform\Lib\HTTP\Results\StakeResult;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\StakeRequestSenderInterface;
use GamingPlatform\Lib\HTTP\Traits\HTTPSendTrait;

class Operator1StakeRequestSender implements StakeRequestSenderInterface
{
    use HTTPSendTrait;
    /**
     * Send request to Operator 1
     *
     * @inheritDoc
     */
    public function send(Money $amount, string $account): StakeResult
    {
        $requestCreator = new StakeRequestCreator($amount, $account);
        $request = $requestCreator->create('operator1');

        try {
            $result = $this->call($request);
        } catch (HTTPException $e) {
            return StakeResult::RESULT_ERROR;
        }

        // The structure can be different between the Operators,
        // so here we fetch the information depending on the structure this Operator offers.
        // Then I put it in a unified result
        return (new StakeResult())
            ->setStatus((
                $result->getStatus() === 200 &&
                ($body = $result->getBody()) &&
                ($body = json_decode($body, true)) &&
                isset($body['status']) &&
                $body['status'] === 'success'
            ) ? StakeResult::RESULT_SUCCESS : StakeResult::RESULT_ERROR);
    }
}
