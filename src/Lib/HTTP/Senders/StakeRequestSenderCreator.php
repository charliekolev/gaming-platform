<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\HTTP\Senders\Interfaces\StakeRequestSenderCreatorInterface;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\StakeRequestSenderInterface;

class StakeRequestSenderCreator implements StakeRequestSenderCreatorInterface
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1StakeRequestSender::class,
        // ....
    ];

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?StakeRequestSenderInterface
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return new $className();
        } else {
            return null;
        }
    }
}
