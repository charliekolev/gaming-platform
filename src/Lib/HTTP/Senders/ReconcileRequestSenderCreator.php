<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\HTTP\Senders\Interfaces\ReconcileRequestSenderCreatorInterface;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\ReconcileRequestSenderInterface;

class ReconcileRequestSenderCreator implements ReconcileRequestSenderCreatorInterface
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1ReconcileRequestSender::class,
        // ....
    ];

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?ReconcileRequestSenderInterface
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return new $className();
        } else {
            return null;
        }
    }
}
