<?php
namespace GamingPlatform\Lib\HTTP\Senders;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\HTTPException;
use GamingPlatform\Lib\HTTP\Requests\WinRequestCreator;
use GamingPlatform\Lib\HTTP\Results\WinResult;
use GamingPlatform\Lib\HTTP\Senders\Interfaces\WinRequestSenderInterface;
use GamingPlatform\Lib\HTTP\Traits\HTTPSendTrait;

class Operator1WinRequestSender implements WinRequestSenderInterface
{
    use HTTPSendTrait;
    /**
     * Send request to Operator 1
     *
     * @inheritDoc
     */
    public function send(Money $amount): WinResult
    {
        $requestCreator = new WinRequestCreator($amount);
        $request = $requestCreator->create('operator1');

        try {
            $result = $this->call($request);
        } catch (HTTPException $e) {
            return WinResult::RESULT_ERROR;
        }

        // Different Operator may return different statuses, and here we map it to unified ones
        $map = [
            'success' => WinResult::RESULT_SUCCESS,
            'error' => WinResult::RESULT_ERROR,
            'retry' => WinResult::RESULT_RETRY,
        ];

        // As well the structure can be different between the Operators,
        // so here we fetch the information depending on the structure this Operator offers.
        // Then I put it in a unified result
        return (new WinResult())
            ->setStatus((
                $result->getStatus() === 200 &&
                ($body = $result->getBody()) &&
                ($body = json_decode($body, true)) &&
                isset($body['status'], $map[$body['status']])
            ) ? $map[$body['status']] : WinResult::RESULT_ERROR);
    }
}
