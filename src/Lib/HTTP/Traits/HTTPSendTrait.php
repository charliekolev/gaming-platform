<?php

namespace GamingPlatform\Lib\HTTP\Traits;

use GamingPlatform\Lib\DependencyInjection\DependencyInjectionService;
use GamingPlatform\Lib\DependencyInjection\DependencyNotFoundException;
use GamingPlatform\Lib\HTTP\HTTP;
use GamingPlatform\Lib\HTTP\HTTPException;
use GamingPlatform\Lib\HTTP\HTTPRequest;
use GamingPlatform\Lib\HTTP\HTTPResult;

trait HTTPSendTrait
{
    /**
     * @param HTTPRequest $request
     *
     * @return HTTPResult
     * @throws HTTPException
     */
    protected function call(HTTPRequest $request): HTTPResult
    {
        try {
            /** @var HTTP $http */
            $http = (new DependencyInjectionService())->get('http');
        } catch (DependencyNotFoundException $e) {
            // TODO: throw an exception
        }

        return $http->send($request);
    }
}
