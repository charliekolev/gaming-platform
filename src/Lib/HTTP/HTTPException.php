<?php

namespace GamingPlatform\Lib\HTTP;

use Exception;

class HTTPException extends Exception
{
    public const GENERIC_EXCEPTION = 0;
    public const CLIENT_EXCEPTION = 1;
    public const SERVER_EXCEPTION = 2;
}
