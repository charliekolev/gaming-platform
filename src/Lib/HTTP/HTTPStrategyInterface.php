<?php

namespace GamingPlatform\Lib\HTTP;

/**
 * An interface to be implemented by all the HTTP request clients
 */
interface HTTPStrategyInterface
{
    /**
     * Send HTTP message
     *
     * @param HTTPRequest $request
     * @return HTTPResult
     * @throws HTTPException
     */
    public function send(HTTPRequest $request): HTTPResult;
}
