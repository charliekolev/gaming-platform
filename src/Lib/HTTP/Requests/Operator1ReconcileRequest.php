<?php
namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\HTTP\HTTPRequest;

class Operator1ReconcileRequest extends HTTPRequest
{
    /**
     * Use all the passed data to the constructor to create the Operator specific request
     */
    public function __construct()
    {
        $this
            ->setMethod('POST')
            // Extra headers and options related to Loss request for Operator 1
            ;
    }
}
