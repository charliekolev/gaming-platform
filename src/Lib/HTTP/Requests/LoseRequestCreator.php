<?php
namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\HTTP\HTTPRequest;

class LoseRequestCreator extends AbstractOperatorRequestCreator
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1LoseRequest::class,
    ];

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?HTTPRequest
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return (new $className())->setURL($this->getOperatorURL($operator));
        } else {
            return null;
        }
    }
}
