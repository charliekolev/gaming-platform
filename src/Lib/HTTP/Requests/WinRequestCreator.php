<?php
namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\HTTPRequest;

class WinRequestCreator extends AbstractOperatorRequestCreator
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1WinRequest::class,
    ];

    private Money $amount;

    /**
     * Pass all the needed options to the constructor,
     * so we can use them in the created instance for the specific Operator.
     * Then the Operator will decide how to use them in the request
     *
     * @param Money $amount
     */
    public function __construct(Money $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?HTTPRequest
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return (new $className($this->amount))->setURL($this->getOperatorURL($operator));
        } else {
            return null;
        }
    }
}
