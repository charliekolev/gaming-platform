<?php
namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\HTTPRequest;

class StakeRequestCreator extends AbstractOperatorRequestCreator
{
    private const OPERATOR_CLASSES = [
        'operator1' => Operator1StakeRequest::class,
    ];

    private float $amount;
    private string $account;

    /**
     * Pass all the needed options to the constructor,
     * so we can use them in the created instance for the specific Operator.
     * Then the Operator will decide how to use them in the request
     *
     * @param Money $amount
     * @param string $account
     */
    public function __construct(Money $amount, string $account)
    {
        $this->amount = $amount;
        $this->account = $account;
    }

    /**
     * @inheritDoc
     */
    public function create(string $operator): ?HTTPRequest
    {
        if (isset(self::OPERATOR_CLASSES[$operator])) {
            $className = self::OPERATOR_CLASSES[$operator];
            return (new $className($this->amount, $this->account))->setURL($this->getOperatorURL($operator));
        } else {
            return null;
        }
    }
}
