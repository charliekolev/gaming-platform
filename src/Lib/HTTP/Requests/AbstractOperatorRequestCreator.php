<?php

namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\DependencyInjection\DependencyInjectionService;
use GamingPlatform\Lib\DependencyInjection\DependencyNotFoundException;
use GamingPlatform\Lib\HTTP\HTTPRequest;

abstract class AbstractOperatorRequestCreator
{
    /**
     * Create the HTML request for a specified operator
     *
     * @param string $operator
     *
     * @return HTTPRequest|null
     */
    abstract public function create(string $operator): ?HTTPRequest;

    /**
     * Get Operator URL
     *
     * @param string $operator
     *
     * @return string
     * @throws DependencyNotFoundException
     */
    protected function getOperatorURL(string $operator): string
    {
        $conf = (new DependencyInjectionService())->get('config');

        if (isset($conf['operators']['host'][$operator])) {
            return $conf['operators']['host'][$operator];
        } else {
            // TODO: throw an exception
        }
    }
}
