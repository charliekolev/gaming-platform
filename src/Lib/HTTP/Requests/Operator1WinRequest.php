<?php
namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\HTTPRequest;

class Operator1WinRequest extends HTTPRequest
{
    /**
     * Use all the passed data to the constructor to create the Operator specific request
     *
     * @param Money $amount
     */
    public function __construct(Money $amount)
    {
        $this
            ->setMethod('POST')
            ->setIsJson(true)
            ->setParams([
                'amount' => $amount->getAmount(),
                'currency' => $amount->getCurrency()
                // Extra parameters related to Win request for Operator 1
            ])
            // Extra headers and options related to Win request for Operator 1
            ;
    }
}
