<?php
namespace GamingPlatform\Lib\HTTP\Requests;

use GamingPlatform\Lib\Generic\Money;
use GamingPlatform\Lib\HTTP\HTTPRequest;

class Operator1StakeRequest extends HTTPRequest
{
    /**
     * Use all the passed data to the constructor to create the Operator specific request
     *
     * @param Money $amount
     * @param string $account
     */
    public function __construct(Money $amount, string $account)
    {
        $this
            ->setMethod('POST')
            ->setIsJson(true)
            ->setParams([
                'account' => $account,
                'amount' => $amount->getAmount(),
                'currency' => $amount->getCurrency(),
                // Extra parameters related to Stake request for Operator 1
            ])
            // Extra headers and options related to Stake request for Operator 1
            ;
    }
}
