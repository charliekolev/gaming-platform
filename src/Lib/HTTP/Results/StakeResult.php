<?php

namespace GamingPlatform\Lib\HTTP\Results;

class StakeResult
{
    public const RESULT_ERROR = 0;
    public const RESULT_SUCCESS = 1;

    public int $status = self::RESULT_ERROR;

    /**
     * Set Status of the result
     *
     * @param int $status
     *
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status of the result
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}
