<?php

namespace GamingPlatform\Lib\DependencyInjection;

use Exception;

class DependencyNotFoundException extends Exception
{
}
