<?php

namespace GamingPlatform\Lib\DependencyInjection;

/**
 * This class is just a wrapper to avoid the use a singleton class.
 */
class DependencyInjectionService
{
    private DependencyContainer $container;

    public function __construct()
    {
        $this->container = DependencyContainer::getInstance();
    }

    /**
     * Registers a dependency into the dependency container
     *
     * @param string $identifier
     *  The identifier of the dependency
     * @param callable $loader
     *  The generator for the dependency object
     * @param bool $singleton
     *  Whether to return always the same instance of the object
     *
     * @return void
     */
    public function register(string $identifier, callable $loader, bool $singleton = true): void
    {
        $this->container->add($identifier, $loader, $singleton);
    }

    /**
     * Gets the dependency by its identifier.
     *
     * @param string $identifier
     *  The identifier of the dependency
     *
     * @return object
     * @throws DependencyNotFoundException
     */
    public function get(string $identifier): object
    {
        return $this->container->get($identifier);
    }
}
