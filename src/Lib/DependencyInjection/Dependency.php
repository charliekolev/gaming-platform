<?php

namespace GamingPlatform\Lib\DependencyInjection;

/**
 * Represents a dependency.
 */
class Dependency
{
    private object $object;
    private bool $singleton;
    private $loader;

    /**
     * Dependency constructor
     *
     * @param callable $loader
     * @param bool $singleton
     */
    public function __construct(callable $loader, bool $singleton = true)
    {
        $this->singleton = $singleton;
        $this->loader = $loader;
    }

    /**
     * Returns the specific dependency instance.
     *
     * @return object
     */
    public function get(): object
    {
        if (!$this->singleton) {
            return call_user_func($this->loader);
        }

        if (!isset($this->object)) {
            $this->object = call_user_func($this->loader);
        }

        return $this->object;
    }
}

