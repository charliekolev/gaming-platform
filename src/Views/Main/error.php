<header>
    <h1>ERROR <?php echo $data['error'] ?? ''; ?></h1>
</header>
<section>
    <div id="container" >
        <p><?php echo $data['message'] ?? ''; ?></p>
    </div>
</section>
