# Gaming Platform
## _Main description_

**Nothing was tested!!! In the code I mainly describe the ideas I have. 
If you need a working code - just tell me, I'll spend some more time, in order to make it a working solution.**

Directory structure is pretty much self-explanatory.
All the settings can be found in the config.ini.

## Task 1: General structure:

I've created a Dependency injection service and container (could use a third party library, 
but I decided to create a very simple one for that case), 
so I can inject the objects when needed to reuse the same object as singletons (Singleton pattern), 
or as separate instance.

All the HTTP requests are sent the HTTP class that implements the Strategy pattern, 
this way we can easily change the HTTP client, and all the requests, 
results and exceptions will have the same structure. 
Currently I've registered the Guzzle client in the Dependency container. 
Some PHP frameworks create a similar abstraction (Laravel has HTTP Facade),
so this functionality can be used out of the box. 

I use Factory method pattern, in order to create in a unified way the Request and Request sender 
for specified Operator and request type. Now we may have different Operator Request structure 
(different headers, body, authentication, request method...), 
witch sent by the specific Sender, creates the same Result structure for that type, 
no matter of the result Operator sends back.

```sh
$senderCreator = new StakeRequestSenderCreator();

// Creates different HTTPRequest (for different Opeartors) from the same Stake Creator
$sender = $senderCreator->create($operator);

// Returns the same StakeRequest object, no matter of the choosen Operator
$result = $sender->send($stakeAmount, $account);
```

## Task 2: Problem definitions:

Here I didn't specify any authentication methods, but we can use any kind. 
I create different request so we can send a Basic authentication, API token in the header or the body, 
OAuth ... and so on, depending on the used Operator. 

The possible networks issues could happen because of failed authentication, 
change in the Operators API request/result structure (usually they should create new API version to avoid that), 
temporary lack of connectivity, connection timeout, rate limits and reaching quotas.

It's possible that message queue worker process gets dropped, and stops sending the asynchronous reconciliation requests.
Here come some process monitors that can be responsible for creating and maintaining the (usually few per tube) processes. 
Supervisor is a good example for such a process monitor.

A good practice is instead of setting the sensitive configuration settings like passwords, API keys and 
DB connection setting directly in the config.ini, to pass them like ${SOME_API_KEY}, 
and then add them as an environment variables in the operating system. 
Some not sensitive information that is different for the environment (production, development, testing),
can be added in a separate .env file and then loaded via the boot loader or the docker composer. 

For good performance we should consider using a good hosting service, reserve enough memory and processor resources,
so we don't get frequent overloads. We could consider using CDN for the assets and fasten the page loading time.
Good caching machinery like Redis or Memcache is mandatory (Redis can be user for a broker in the message queues as well
if we decide using another queue library). 

Should write a lot of unit and functional tests (here I didn't, not sure if it was a part of the task, 
I was concentrated on the architecture), a/nd preferably use TDD, so we create a stable platform from the very beginning 
and not don't break any functionality in the meantime when developing new features.

## Task 3: Reconciliation process:
_(Worker consumer is placed in console/worker.php)_

I've used the Pheanstalk client for Beanstalkd queue, but if we decide using Laravel, it comes with very strong 
and fine adjustable queuing mechanism that can use database, Amazon SQS, Redis, and Beanstalkd as a driver. 
Another old-fashioned approach could be by using the cron, but there is a down-side that we can't send the first request 
immediately, or we can send it synchronously (if we need realtime-result with all the blocking consequences).

## Task 4: Security issues:

The possible security risks could be:

#### Stealing or watching files
In order non-authorizer persons to not be able to see the code or the configurations, 
it's good to put all the non-public files out 
of the HTTP public folder. Should never keep passwords and other sensitive information in the files, 
but rather as an environment variables.

#### Network authentication and security
On the transport level its best to rely on SSL or TLS connections. If possible we should prefer using
OAuth and JWT authentications. It's good to involve firewall, so we admit only connections to/from the Operators 
and services we use. 

#### Operator predefined limits

We could add some limits that we think that shouldn't be crossed when sending/receiving data from the Operators,
like maximum money amount and available currencies, unexpected data structures ...

#### DB
For best performance DB server should either reside in the same server together with the HTTP server, 
or be in the same network. If not locally then the firewall should be well preset on both machines.
When savin in the DB, hashing passwords is a must. If MySQL used, we can use InnoDB table encryption. 
MariaDB has its own methods of encryption as well, but we should be careful about the performance.
To avoid injection we should use either some ORM ,or/and good DB builder, or PDO but take a good care of using 
the placeholders, and make form validations.

#### Sessions

Define reasonable session timeout, and session encryption. Regenerate session id after successful authentication.
Use CSRF token for the forms, and limit the number of logging attempts. Turn on the cookie_httponly flag in the php.ini.
And extra saving/checking against IP and user agent string adds extra security.
#### Cookies

Never store personal data and sensitive information in cookies, 
restrict to specific path when possible, and add short expiration time 
(or leave to default - to delete when browser gets closed).
