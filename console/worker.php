<?php

require_once __DIR__ . '/../basics.php';

use GamingPlatform\Lib\HTTP\Results\ReconcileResult;
use GamingPlatform\Lib\HTTP\Senders\ReconcileRequestSenderCreator;
use Pheanstalk\Pheanstalk;
use GamingPlatform\Lib\DependencyInjection\DependencyInjectionService;
use GamingPlatform\Lib\DependencyInjection\DependencyNotFoundException;

try {
    $conf = (new DependencyInjectionService())->get('config');
} catch (DependencyNotFoundException $e) {
    echo "Not possible to get the Platform configuration!\n";
}

if (
    !empty($conf['pheanstalk']['host']) &&
    !empty($conf['pheanstalk']['reconciliation_tube'])
) {
    $pheanstalk = Pheanstalk::create($conf['pheanstalk']['host']);
    $pheanstalk->watch($conf['pheanstalk']['reconciliation_tube']);

    while (true) {
        $job = $pheanstalk->reserve();

        if (
            isset($job) &&
            ($data = $job->getData()) &&
            ($data = json_decode($data, true)) &&
            !empty($data['operator'])
        ) {
            $senderCreator = new ReconcileRequestSenderCreator();
            $sender = $senderCreator->create($data['operator']);
            $result = $sender->send();
            $status = $result->getStatus();

            if ($status === ReconcileResult::RESULT_SUCCESS) {
                $pheanstalk->delete($job);
                // TODO: do something on success - could be creating a log message
            } elseif ($status === ReconcileResult::RESULT_ERROR) {
                $pheanstalk->delete($job);

                $data['wait'] = empty($data['wait']) ? 30 : $data['wait'] * 2;
                $data['tries'] = empty($data['tries']) ? 1 : $data['tries'] + 1;

                if ($data['tries'] < 10) {
                    $pheanstalk
                        ->useTube($conf['pheanstalk']['reconciliation_tube'])
                        ->put(json_encode(['data' => $data], JSON_THROW_ON_ERROR), $data['wait']);
                }
            } else {
                // TODO: retry -  I dont know what to do in that case
            }
        } else {
            $pheanstalk->delete($job);
            // TODO: do something on broken data:
            // Could be creating a log message
            // or writing to DB for manual cancellation/refund
        }
    }
} else {
    echo "Pheanstalk configuration is not available!\n";
}

